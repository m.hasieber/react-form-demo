# react-form-demo

## Usage

```console
yarn
yarn dev
```

Replace `import { App } from "./app";` in `./src/main.tsx` with `import { App } from "./app2";` to test schema validation with `zod`!

See <https://denovo-gmbh.atlassian.net/l/cp/GgVeY9AN> for more information.
