import { FC } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { Form } from "./form/form";

export interface IFormInput {
  firstName: string;
  middleName: string;
  lastName: string;
  email: string;
  password: string;
  password2: string;
  age: number;
}

const DEFAULT_VALUES: IFormInput = {
  firstName: "Marcel",
  middleName: "",
  lastName: "Hasieber",
  email: "m.hasieber@denovo.at",
  password: "secret",
  password2: "secret",
  age: 24,
};

export const App: FC = () => {
  const formMethods = useForm<IFormInput>({ defaultValues: DEFAULT_VALUES, mode: "all" });

  return (
    <FormProvider {...formMethods}>
      <Form />
    </FormProvider>
  );
};
