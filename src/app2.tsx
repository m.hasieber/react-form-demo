import { FC } from "react";
import { FormProvider, useForm } from "react-hook-form";
import { z } from "zod";
import { Form } from "./form/form2";
import { zodResolver } from "@hookform/resolvers/zod";

export interface IFormInput {
  firstName: string;
  middleName: string;
  lastName: string;
  email: string;
  age: number;
}

const schema = z.object({
  firstName: z.string().min(1).max(20),
  middleName: z.string().optional(),
  lastName: z.string().regex(/^[A-Za-z]+$/i),
  email: z.string().email(),
  age: z.number().min(18).max(99),
});

export const App: FC = () => {
  const formMethods = useForm<IFormInput>({ resolver: zodResolver(schema) });

  return (
    <FormProvider {...formMethods}>
      <Form />
    </FormProvider>
  );
};
