import { FC } from "react";
import { useFormContext } from "react-hook-form";
import { IFormInput } from "../app";

interface AgeProps {}

export const Age: FC<AgeProps> = (props) => {
  const {
    register,
    formState: { errors },
  } = useFormContext<IFormInput>();

  return (
    <div>
      <input
        type="number"
        style={{ border: errors["age"] ? "1px red solid" : "" }}
        {...register("age", { min: 18, max: 99 })}
      />
    </div>
  );
};
