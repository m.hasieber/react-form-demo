import { FC } from "react";
import { SubmitHandler, useFormContext } from "react-hook-form";
import { IFormInput } from "../app";
import { Age } from "./age";
import { Email } from "./email";
import { Name } from "./name";
import { Password } from "./password";

interface FormProps {}

export const Form: FC<FormProps> = (props) => {
  const { handleSubmit, reset } = useFormContext<IFormInput>();

  const onSubmit: SubmitHandler<IFormInput> = (input) => {
    console.log("submit", input);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Name />
      <Email />
      <Age />
      <Password />

      <input type="button" onClick={() => reset()} value="Reset" />
      <input type="submit" />
    </form>
  );
};
