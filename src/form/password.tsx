import { FC } from "react";
import { useFormContext } from "react-hook-form";
import { IFormInput } from "../app";

interface PasswordProps {}

export const Password: FC<PasswordProps> = (props) => {
  const {
    register,
    getValues,
    formState: { errors },
  } = useFormContext<IFormInput>();
  return (
    <div>
      <input
        placeholder="Password*"
        type={"password"}
        style={{ border: errors["password"] ? "1px red solid" : "" }}
        {...register("password", {
          required: true,
          minLength: 1,
          deps: ["password2"],
          validate: (value) => value === getValues("password2"),
        })}
      />

      <input
        placeholder="Confirm Password*"
        type={"password"}
        style={{ border: errors["password2"] ? "1px red solid" : "" }}
        {...register("password2", {
          required: true,
          deps: ["password"],
          validate: (value) => value === getValues("password"),
        })}
      />
    </div>
  );
};
