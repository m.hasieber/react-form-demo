import { FC } from "react";
import { SubmitHandler, useFormContext } from "react-hook-form";
import { IFormInput } from "../app";

interface FormProps {}

export const Form: FC<FormProps> = (props) => {
  const {
    handleSubmit,
    register,
    formState: { errors },
    reset
  } = useFormContext<IFormInput>();

  const onSubmit: SubmitHandler<IFormInput> = (input) => {
    console.log("submit", input);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <div>
        <input placeholder="First name*" {...register("firstName")} />
        <input placeholder="Last name" {...register("middleName")} />
        <input placeholder="Last name*" {...register("lastName")} />
      </div>

      <div>
        <input
          placeholder="Email"
          style={{ border: errors["email"] ? "1px red solid" : "" }}
          {...register("email")}
        />
        {errors["email"] && (
          <span>
            <p>{errors["email"].message}</p>
          </span>
        )}
      </div>

      <div>
        <input
          type="number"
          style={{ border: errors["age"] ? "1px red solid" : "" }}
          {...register("age", { valueAsNumber: true })}
        />
        <span>
          <p>{errors?.age?.message}</p>
        </span>
      </div>

      <input type="button" onClick={() => reset()} value="Reset" />
      <input type="submit" />
    </form>
  );
};
