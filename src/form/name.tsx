import { FC } from "react";
import { useFormContext } from "react-hook-form";
import { IFormInput } from "../app";

interface NameProps {}

export const Name: FC<NameProps> = (props) => {
  const { register } = useFormContext<IFormInput>();
  return (
    <div>
      <input
        placeholder="First name*"
        {...register("firstName", { required: true, maxLength: 20 })}
      />

      <input placeholder="Last name" {...register("middleName")} />

      <input
        placeholder="Last name*"
        {...register("lastName", { required: true, pattern: /^[A-Za-z]+$/i })}
      />
    </div>
  );
};
