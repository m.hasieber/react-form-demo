import { FC } from "react";
import { useFormContext } from "react-hook-form";
import { IFormInput } from "../app";

interface EmailProps {}

export const Email: FC<EmailProps> = (props) => {
  const {
    register,
    formState: { errors },
    resetField,
    watch,
  } = useFormContext<IFormInput>();

  const mail = watch("email");
  console.log(mail);

  const isEmail = (value: string) => {
    if (!value.includes("@")) return "Email must include @!";
    if (value.split("@").filter(Boolean).length !== 2)
      return "Email must have values before and after @!";
    if (!value.includes(".")) return "Email must contain a dot!";
    if (value.endsWith(".")) return "Email must contain top level domain!";
    return undefined;
  };

  return (
    <div>
      <input
        placeholder="Email"
        style={{ border: errors["email"] ? "1px red solid" : "" }}
        {...register("email", {
          required: true,
          maxLength: 20,
          validate: isEmail,
        })}
      />

      <input type="button" onClick={() => resetField("email")} value="Reset" />

      {errors["email"] && (
        <span>
          <p>{errors["email"].message}</p>
        </span>
      )}
    </div>
  );
};
